import Gtk from 'gi://Gtk'
import Adw from 'gi://Adw'

import {ExtensionPreferences} from 'resource:///org/gnome/Shell/Extensions/js/extensions/prefs.js'

export default class VerticalWindowListExtensionPreferences extends ExtensionPreferences {
    fillPreferencesWindow(window) {
        const settings = this.getSettings()
        const page = new Adw.PreferencesPage()
        const group = new Adw.PreferencesGroup({
            title: this.metadata.name + ' preferences'
        })
        page.add(group)

        const title = new Gtk.Label({
            label: `<b>${this.metadata.name} Preferences</b>`,
            use_markup: true,
            valign: Gtk.Align.CENTER,
            hexpand: true
        })
        group.add(title, 0, 0, 2, 1)

        // width
        const widthLabel = new Gtk.Label({
            label: 'Panel width'
        })
        group.add(widthLabel, 0, 1, 1, 1)
        const width = new Gtk.Scale({
            orientation: Gtk.Orientation.HORIZONTAL,
            draw_value: true,
            digits: 0
        })
        width.set_range(10, 1000)
        group.add(width, 1, 1, 1, 1)
        width.connect('value-changed', function () { settings.set_int('width', Math.floor(width.get_value())) })
        settings.connect('changed::width', function () { width.set_value(settings.get_int('width')) })
        settings.emit('changed::width', '')

        // icon-size
        const iconLabel = new Gtk.Label({
            label: 'Icon size'
        })
        group.add(iconLabel, 0, 2, 1, 1)
        const iconSize = new Gtk.Scale({
            orientation: Gtk.Orientation.HORIZONTAL,
            draw_value: true,
            digits: 0
        })
        iconSize.set_range(8, 256)
        group.add(iconSize, 1, 2, 1, 1)
        iconSize.connect('value-changed', function () { settings.set_int('icon-size', Math.floor(iconSize.get_value())) })
        settings.connect('changed::icon-size', function () { iconSize.set_value(settings.get_int('icon-size')) })
        settings.emit('changed::icon-size', '')

        // opacity
        const opacityLabel = new Gtk.Label({
            label: 'Opacity'
        })
        group.add(opacityLabel, 0, 3, 1, 1)
        const opacity = new Gtk.Scale({
            orientation: Gtk.Orientation.HORIZONTAL,
            draw_value: true,
            digits: 0
        })
        opacity.set_range(0, 255)
        group.add(opacity, 1, 3, 1, 1)
        opacity.connect('value-changed', function () { settings.set_int('panel-opacity', Math.floor(opacity.get_value())) })
        settings.connect('changed::panel-opacity', function () { opacity.set_value(settings.get_int('panel-opacity')) })
        settings.emit('changed::panel-opacity', '')
        window.add(page)


        //display windows from all workspaces
        const showAllWorkspacesCheckbox = new Gtk.CheckButton({
            label: 'Show windows from all workspaces'
        })
        group.add(showAllWorkspacesCheckbox, 0, 4, 1, 1)
        showAllWorkspacesCheckbox.connect('toggled', function () { settings.set_boolean('show-all-workspaces',showAllWorkspacesCheckbox.get_active()) })
        settings.connect('changed::show-all-workspaces', function () { showAllWorkspacesCheckbox.set_active(settings.get_boolean('show-all-workspaces')) })
        settings.emit('changed::show-all-workspaces', '')
    }

}

