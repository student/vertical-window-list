import St from 'gi://St'
import Meta from 'gi://Meta'
import Clutter from 'gi://Clutter'
import Shell from 'gi://Shell'
import Mtk from 'gi://Mtk'

import * as DND from 'resource:///org/gnome/shell/ui/dnd.js'
import * as Main from 'resource:///org/gnome/shell/ui/main.js'
import * as PopupMenu from 'resource:///org/gnome/shell/ui/popupMenu.js'
import * as AppFavorites from 'resource:///org/gnome/shell/ui/appFavorites.js'
import * as BoxPointer from 'resource:///org/gnome/shell/ui/boxpointer.js'

import {Extension} from 'resource:///org/gnome/shell/extensions/extension.js'

const CYCLING_TIMEOUT_MS = 1000
const APPLICATION_ICON_SIZE = 32
const DEVELOP = false

export const log = function (message) {
    if (DEVELOP) {
        console.log('VWL: ' + message)
    }
}


export default class VerticalWindowListExtension extends Extension {
    enable() {
        this.enabled = true

        this.settings = this.getSettings()

        this.app_system = Shell.AppSystem.get_default()

        this.monitor = Main.layoutManager.primaryMonitor

        this.window_panel = new St.Widget({
            style_class: 'windows-panel',
            reactive: true,
            track_hover: true,
            visible: true,
            layout_manager: new Clutter.BoxLayout({
                orientation: Clutter.Orientation.VERTICAL
            }),
            height: this.monitor.height || 1000,
            width: this.settings.get_int('width')
        })

        this.window_panel.set_position(this.monitor.x || 0, 30)

        this.window_list = new St.Widget({
            style_class: 'windows-list',
            reactive: true,
            layout_manager: new Clutter.BoxLayout({
                homogeneous: true,
                orientation: Clutter.Orientation.VERTICAL
            }),
            y_align: Clutter.ActorAlign.START,
            x_expand: false,
            y_expand: false
        })

        this.spacer_widget = new St.Label({
            reactive: true,
            text: '',
            y_align: Clutter.ActorAlign.FILL,
            x_expand: true,
            y_expand: true
        })

        // non-transparent text and icons
        this.window_list.set_opacity_override(255)
        this.favorites_list = null
        this.window_tracker = Shell.WindowTracker.get_default()
        this.saved_windows = JSON.parse(this.settings.get_string('windows') || '{}')

        // cycle windows keybingings
        this.cycling = 0
        Main.wm.addKeybinding('cycle-up', this.settings, Meta.KeyBindingFlags.NONE, Shell.ActionMode.NORMAL, this.cycleWindow.bind(this, true))
        Main.wm.addKeybinding('cycle-down', this.settings, Meta.KeyBindingFlags.NONE, Shell.ActionMode.NORMAL, this.cycleWindow.bind(this, false))
        this.window_panel.connect('destroy', () => {
            Main.wm.removeKeybinding('cycle-up')
            Main.wm.removeKeybinding('cycle-down')
        })

        {
            const signal = this.settings.connect('changed::panel-opacity', this.opacity_changed.bind(this))
            this.window_panel.connect('destroy', () => { this.settings.disconnect(signal) })
        }

        {
            const signal = this.settings.connect('changed::show-all-workspaces', this.show_all_workspaces_changed.bind(this))
            this.window_panel.connect('destroy', () => { this.settings.disconnect(signal) })
        }

        {
            const signal = this.settings.connect('changed::width', this.width_changed.bind(this))
            this.window_panel.connect('destroy', () => { this.settings.disconnect(signal) })
        }

        {
            const signal = this.spacer_widget.connect('button-press-event', this.spacer_clicked.bind(this))
            this.window_panel.connect('destroy', () => { this.window_panel.disconnect(signal) })
        }

        {
            const signal = this.window_panel.connect('scroll-event', this.panel_scrolled.bind(this))
            this.window_panel.connect('destroy', () => { this.window_panel.disconnect(signal) })
        }

        {
            const signal = global.workspace_manager.connect('active-workspace-changed', this.add_workspace_windows.bind(this))
            this.window_panel.connect('destroy', () => { global.workspace_manager.disconnect(signal) })
        }

        {
            const signal = Main.layoutManager.connect('monitors-changed', () => {
                this.monitor = Main.layoutManager.primaryMonitor
                this.window_panel.set_position(this.monitor?.x || 0, 30)
                this.window_panel.set_height(this.monitor?.height || 1000)
                Main.layoutManager.removeChrome(this.window_panel)
                return Main.layoutManager.addChrome(this.window_panel, {
                    affectsStruts: true,
                    trackFullscreen: true
                })
            })
            this.window_panel.connect('destroy', () => { Main.layoutManager.disconnect(signal) })
        }

        this.window_panel.add_child(this.window_list)
        this.window_panel.add_child(this.spacer_widget)
        Main.layoutManager.addChrome(this.window_panel, {
            affectsStruts: true,
            trackFullscreen: true
        })

        this.add_workspace_windows()
        this.opacity_changed()

        this._previous_window_back = false

    }

    disable() {
        this.enabled = false
        this.save_workspace_windows()
        this.settings.set_string('windows', JSON.stringify(this.saved_windows))
        this.window_panel.destroy()


        this.window_panel = null
        this.settings = null
        this.app_system = null
        this.monitor = null
        this.window_list = null
        this.spacer_widget = null
        this.favorites_list = null
        this.window_tracker = null
        this.saved_windows = null
        this.cycling = null

    }


    add_workspace_windows() {
        if (!this.enabled) return

        const workspace = global.workspace_manager.get_active_workspace()
        this.save_workspace_windows()
        for (const button of this.window_list.get_children()) {
            button.destroy()
        }

        let windows_present = []
        for (const metaWindow of this.workspace_windows()) {
            const windowPresent = {
                order: metaWindow.get_stable_sequence(),
                windowId: metaWindow.get_id(),
                metaWindow: metaWindow,
                workspaceIndex: metaWindow.get_workspace()?.index() || -1
            }
            const windowSaved = this.saved_windows[windowPresent.windowId]
            if (windowSaved) {
                windowPresent.order = windowSaved.order
                windowPresent.customTitle = windowSaved.customTitle
            }
            windows_present.push(windowPresent)
        }
        windows_present.sort(function (a, b) {
            return 0 + (a.order >= b.order) - (a.order <= b.order)
        })
        for (const w of windows_present) {
            this.add_window(workspace, w.metaWindow, w.customTitle)
        }

        if (workspace._window_added == null) {
            workspace._window_added = workspace.connect_after('window-added', this.add_window.bind(this))
        }

        if (workspace._window_removed == null) {
            workspace._window_removed = workspace.connect_after('window-removed', this.remove_window.bind(this))
        }
    }

    cycleWindow(up, display, window, binding) {
        if (!this.enabled) return

        let b, buttons

        // cycle up or down the list starting from previous Window
        buttons = this.window_list.get_children()
            let [activeWindow, ix] = ((function () {
            let i, len, results
            results = []
            for (ix = i = 0, len = buttons.length; i < len; ix = ++i) {
                b = buttons[ix]
                if (b.has_style_class_name('focused')) {
                    results.push([b.metaWindow, ix])
                }
            }
            return results
        })())[0] || []
        if (activeWindow == null) {
            return
        }
        if (Date.now() - this.cycling > CYCLING_TIMEOUT_MS) {
            this.activate_window(this.find_previous_window())
            this.cycling = Date.now()
            return
        }
        if (up) {
            ix = (ix === buttons.length - 1) ? 0 : ix + 1
        } else {
            ix = ix === 0 ? buttons.length - 1 : ix - 1
        }
        this.activate_window(buttons[ix].metaWindow)
        this.cycling = Date.now()
    }

    spacer_clicked(spacer, event) {
        if (event.get_button() === 1) {
            if (this.favorites_list?.isOpen) {
                this.hide_favorites()
            } else {
                this.show_favorites(event)
            }
        }
        if (event.get_button() === 2) {
            this.show_settings()
        }
        return Clutter.EVENT_STOP
    }

    panel_scrolled(panel, event) {
        const buttons = this.window_list.get_children()
        if (buttons.length < 2) {
            return Clutter.EVENT_STOP
        }

        const current_window = global.display.get_tab_current(Meta.TabList.NORMAL, global.workspace_manager.get_active_workspace())

        let ix = 0
        for (const button of buttons) {
            if (button.metaWindow === current_window) {
                if (event.get_scroll_direction() == Clutter.ScrollDirection.UP) {
                    this.activate_window(buttons[ix > 0? ix-1 : buttons.length-1].metaWindow)
                }
                if (event.get_scroll_direction() == Clutter.ScrollDirection.DOWN) {
                    this.activate_window(buttons[(ix < buttons.length-1) ? ix + 1 : 0].metaWindow)
                }
            }
            ix += 1
        }

        return Clutter.EVENT_STOP
    }

    show_settings() {
        try {
            this.openPreferences()
        }
        catch (e) {
            log('Open settings error: ' + e)
        }
    }

    show_favorites(event) {
        if (!this.enabled) return

        let actor, item,  x, y

        this.hide_favorites()
        // apply custom title if it was in edit mode
        for (const btn of this.window_list?.get_children()) {
            if (btn.custom_title.visible) {
                this.update_window_title(btn.metaWindow, btn)
            }
        }
        [x, y] = event.get_coords()
        actor = new St.Widget({
            width: 0,
            height: 0,
            opacity: 0
        })
        this.favorites_list = new PopupMenu.PopupMenu(actor, 0, St.Side.TOP)
        const apps = AppFavorites.getAppFavorites().getFavorites()
        if (!apps) {
            return
        }
        for (const app of apps) {
            item = new PopupMenu.PopupImageMenuItem(app.get_name(), app.create_icon_texture(APPLICATION_ICON_SIZE).get_gicon())
            // PopupMenu closes and disconnects signal automatically
            item.connect('activate', () => {
                app.open_new_window(-1)
                // app.activate()
            })

            this.favorites_list.addMenuItem(item)
        }
        Main.layoutManager.addTopChrome(this.favorites_list.actor)
        this.favorites_list.open(BoxPointer.PopupAnimation.NONE)
        // move favorites higher when it does not fit vertically and click was in lower half of monitor
        if (y > this.monitor.height / 2 && y + this.favorites_list.actor.get_height() > this.monitor.height) {
            y = this.monitor.height - this.favorites_list.actor.get_height() - 10
        }
        this.favorites_list.actor.set_x(x + 10)
        this.favorites_list.actor.set_y(y + 10)
    }

    hide_favorites() {
        if (this.favorites_list?.isOpen) {
            this.favorites_list?.destroy()
            this.favorites_list = null
        }
    }

    width_changed() {
        this.window_panel.width = this.settings.get_int('width')
    }

    opacity_changed() {
        this.window_panel.set_opacity(this.settings.get_int('panel-opacity'))
    }

    show_all_workspaces_changed() {
        this.add_workspace_windows()
    }

    workspace_windows() {
        return global.display.get_tab_list(Meta.TabList.NORMAL,
            this.settings.get_boolean('show-all-workspaces') ? null : global.workspace_manager.get_active_workspace())
    }


    add_window(workspace, metaWindow, customTitle = '') {
        if (!this.enabled) return

        let button, texture_cache

        if (this.workspace_windows().indexOf(metaWindow) < 0) {
            return
        }

        this.remove_window(workspace, metaWindow)

        button = new St.Widget({
            style_class: 'window-button',
            can_focus: true,
            x_expand: true,
            x_align: Clutter.ActorAlign.FILL,
            reactive: true,
            layout_manager: new Clutter.BoxLayout({
                orientation: Clutter.Orientation.HORIZONTAL
            })
        })
        button.metaWindow = metaWindow
        button.title = new St.Label({
            x_expand: true,
            y_align: Clutter.ActorAlign.CENTER,
            text: '',
            style_class: 'window-button-text'
        })
        button.title.clutter_text.single_line_mode = true
        button.icon_bin = new St.Bin({
            x_expand: false,
            style_class: 'window-button-icon'
        })
        button.custom_title = new St.Entry({
            y_align: Clutter.ActorAlign.CENTER,
            text: customTitle,
            style_class: 'window-button-editor',
            visible: false
        })
        button.add_child(button.icon_bin)
        button.add_child(button.title)
        button.add_child(button.custom_title)

        this.window_list.add_child(button)
        this.update_window_title(metaWindow, button)
        this.updateIcon(metaWindow, button)
        {
            // apply custom title
            const signal = button.custom_title.connect('key-release-event', (custom_title, event) => {
                const keycode = event.get_key_code()
                if (keycode === 36 || keycode === 104) { // Enter
                    this.update_window_title(metaWindow, button)
                }
                if (keycode === 9) { // Escape
                    button.custom_title.text = ''
                    this.update_window_title(metaWindow, button)
                }
            })
            button.connect('destroy', () => { button.custom_title.disconnect(signal) })
        }

        // windows reordering via drag and drop
        button.draggable = DND.makeDraggable(button, {
            dragActorOpacity: 127
        })
        button.draggable.connect('drag-end', this.drag_end.bind(this, button))
        button.draggable.connect('drag-begin', function () {
            let dragActor = new St.Button({
                style_class: 'drag-button',
                label: '' + metaWindow.title
            })

            let [sizeW, sizeH] = button.get_size()
            dragActor.set_size(sizeW, sizeH)

            let [posX, posY] = button.get_transformed_position()
            dragActor.set_position(posX, posY)
            dragActor.getDragActor = function () {
                return dragActor
            }
            button._delegate = dragActor

        })

        {
            const signal = button.connect('button-release-event', this.button_clicked.bind(this))
            button.connect('destroy', () => { button.disconnect(signal) })
        }

        {
            let texture_cache = St.TextureCache.get_default()
            const signal = texture_cache.connect_after('icon-theme-changed', this.updateIcon.bind(this, metaWindow, button))
            button.connect('destroy', () => { texture_cache.disconnect(signal) })
        }

        {
            const signal = metaWindow.connect_after('notify::wm-class', this.updateIcon.bind(this, metaWindow, button))
            button.connect('destroy', () => { metaWindow.disconnect(signal) })
        }

        {
            const signal = this.app_system.connect('app-state-changed', this.updateIcon.bind(this, metaWindow, button))
            button.connect('destroy', () => { this.app_system.disconnect(signal) })
        }


        {
            const signal = metaWindow.connect_after('notify::gtk-application-id', this.updateIcon.bind(this, metaWindow, button))
            button.connect('destroy', () => { metaWindow.disconnect(signal) })
        }

        {
            const signal = this.settings.connect('changed::icon-size', this.updateIcon.bind(this, metaWindow, button))
            button.connect('destroy', () => { this.settings.disconnect(signal) })
        }

        {
            const signal = metaWindow.connect_after('notify::title', this.update_window_title.bind(this, metaWindow, button))
            button.connect('destroy', function () { metaWindow.disconnect(signal) })
        }

        {
            const signal = this.app_system.connect('app-state-changed', this.update_window_title.bind(this, metaWindow, button))
            button.connect('destroy', () => { this.app_system.disconnect(signal) })
        }

        {
            const signal = global.display.connect('notify::focus-window', this.focus_window.bind(this, button))
            button.connect('destroy', () => { global.display.disconnect(signal) })
        }
    }

    drag_end(button) {
        if (!this.enabled) return

        let dropRect = new Mtk.Rectangle({
            x: button.draggable._dragX,
            y: button.draggable._dragY,
            width: 1,
            height: 1
        })

        let bRect

        for (const [ix, b] of this.window_list.get_children().entries()) {
            bRect = new Mtk.Rectangle({
                x: b.get_transformed_position()[0],
                y: b.get_transformed_position()[1],
                width: b.get_transformed_size()[0],
                height: b.get_transformed_size()[1]
            })
            if (b === button) {
                continue
            }
            if (bRect.contains_rect(dropRect)) {
                this.window_list.remove_child(button)
                this.window_list.insert_child_at_index(button, ix)
            }
        }

        // drag under last window
        if (dropRect.y > bRect.y) {
            this.window_list.remove_child(button)
            return this.window_list.add_child(button)
        }
    }

    remove_window(workspace, metaWindow) {
        if (!this.enabled) {
            return
        }

        for (const button of this.window_list.get_children()) {
            if (button.metaWindow === metaWindow) {
                button.destroy()
            }
        }
    }

    focus_window(button) {
        if (global.display.focus_window === button.metaWindow) {
            button.add_style_class_name('focused')
        } else {
            button.remove_style_class_name('focused')
        }
    }

    update_window_title(metaWindow, button) {
        button.title.set_text(button.custom_title.text || ('' + metaWindow.title))
        button.custom_title.visible = false
        button.title.visible = true
    }

    app(metaWindow) {
        return this.window_tracker.get_window_app(metaWindow)
    }

    updateIcon(metaWindow, button) {
        const app = this.app(metaWindow)
        if (app) {
            button.icon_bin.child = app.create_icon_texture(this.settings.get_int('icon-size'))
        }
        if (button.icon_bin.child == null) {
            button.icon_bin.child = new St.Icon({
                icon_name: 'icon-missing',
                icon_size: this.settings.get_int('icon-size')
            })
        }

    }

    button_clicked(button, event) {
        if (!this.enabled) return

        const metaWindow = button.metaWindow

        this.hide_favorites()
        // apply custom title if it was in edit mode
        for (const btn of this.window_list.get_children()) {
            if (btn.custom_title.visible) {
                this.update_window_title(metaWindow, btn)
            }
        }

        // activate previous window if already was active
        const button_number = event.get_button()
        // click through when close to left border
        if (event.get_coords()[0] < 10) {
            this.show_favorites(event)
            return Clutter.EVENT_STOP
        }

        // activate window
        if (button_number === 1) {
            if (metaWindow.appears_focused && metaWindow.get_workspace() == global.workspace_manager.get_active_workspace() ) {
                this.activate_window(this.find_previous_window())
                this._previous_window_back = ! this._previous_window_back
            } else {
                this.activate_window(metaWindow)
                this._previous_window_back = false
            }
            return Clutter.EVENT_STOP
        }

        // edit custom title
        if (button_number === 2) {
            button.title.visible = false
            button.custom_title.visible = true
            button.custom_title.x_expand = true
            button.custom_title.grab_key_focus()
            return Clutter.EVENT_STOP
        }

        // close window on right click
        if (button_number === 3) {
            this.activate_window(metaWindow)
            metaWindow.delete(global.get_current_time())
            return Clutter.EVENT_STOP
        }
    }

    find_previous_window() {
        const current_window = global.display.get_tab_current(Meta.TabList.NORMAL, global.workspace_manager.get_active_workspace())
        return global.display.get_tab_next(Meta.TabList.NORMAL, current_window.get_workspace(), current_window, this._previous_window_back)
    }

    activate_window(metaWindow) {
        if (!this.enabled) return
        if (!metaWindow) return

        const workspace = metaWindow.get_workspace()
        workspace.activate_with_focus(metaWindow, global.get_current_time())

        this.cycling = 0
    }

    save_workspace_windows() {
        for (const [ix, button] of this.window_list.get_children().entries()) {
            const metaWindow = button.metaWindow
            this.saved_windows[metaWindow.get_id()] = {
                order: ix + 1,
                customTitle: button.custom_title.text,
                workspaceIndex: metaWindow.get_workspace()?.index() || -1
            }
        }

    }

}
